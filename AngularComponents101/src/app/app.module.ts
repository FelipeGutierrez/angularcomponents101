import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProgreesBarComponent } from './Components/progrees-bar/progrees-bar.component';
import { CountdownComponent } from './Components/countdown/countdown.component';

@NgModule({
  declarations: [
    AppComponent,
    ProgreesBarComponent,
    CountdownComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
