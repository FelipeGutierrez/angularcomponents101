import { Component, OnInit, Input, Output, OnDestroy, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.css']
})
export class CountdownComponent implements OnInit, OnDestroy, OnChanges {

  @Input() init:number = null;
  public counter:number = 0;

  @Output() onDecrease = new EventEmitter<number>();
  @Output() onComplete = new EventEmitter<void>();

  private countdownTimerRef:any = null;

  constructor() { }

  ngOnInit():void{
    this.startCountdown();
  }

  ngOnDestroy():void{
    this.clearTimerOut();
  }

  ngOnChanges():void{
    this.startCountdown();
  }

  startCountdown(){
    if(this.init && this.init > 0) {
      this.clearTimerOut();
      this.counter = this.init;
      this.doCountdown();
    }
  }

  doCountdown(){
    this.countdownTimerRef = setTimeout(() => {
      this.counter = this.counter -1;
      this.processCountdown();
    }, 1000)
  }

  private clearTimerOut(){
    if(this.countdownTimerRef){
      clearTimeout(this.countdownTimerRef);
      this.countdownTimerRef = null;
    }
  }

  processCountdown(){
    // emit event count
    this.onDecrease.emit(this.counter);

    if(this.counter == 0) {
      this.onComplete.emit();
    } else{
      this.doCountdown();
    }
  }
}
