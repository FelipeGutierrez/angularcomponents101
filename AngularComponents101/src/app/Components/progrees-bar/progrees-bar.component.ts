import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progrees-bar',
  templateUrl: './progrees-bar.component.html',
  styleUrls: ['./progrees-bar.component.css']
})
export class ProgreesBarComponent implements OnInit {

  @Input() progress:number = 0; 

  constructor() { }

  ngOnInit() {
  }

}
