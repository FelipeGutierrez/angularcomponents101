import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgreesBarComponent } from './progrees-bar.component';

describe('ProgreesBarComponent', () => {
  let component: ProgreesBarComponent;
  let fixture: ComponentFixture<ProgreesBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgreesBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgreesBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
